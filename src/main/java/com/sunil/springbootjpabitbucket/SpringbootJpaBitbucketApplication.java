package com.sunil.springbootjpabitbucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.sunil")
public class SpringbootJpaBitbucketApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootJpaBitbucketApplication.class, args);
	}

}
